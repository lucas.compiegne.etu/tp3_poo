public class Rectangle
{
    /*
    Définition des variables
    */
    private double largeur;
    private double longueur;

    /*
    Constructeur pour initialiser la longueur et la largeur du rectangle
    */
    public Rectangle(double L, double l){
        this.largeur = l;
        this.longueur = L;
    }

    /*
    Méthode pour avoir la largeur
    */
    public double getLargeur(){ return this.largeur; }

    /*
    Méthode pour avoir la longueur
    */
    public double getLongueur(){ return this.longueur;}

    /*
    Méthode pour calculer l'aire
    */
    public double Aire(){ return this.largeur*this.longueur; }

    /*
    Méthode pour calculer le périmètre
    */
    public double Perim(){ return (2*(this.largeur+this.longueur)); }

    /*
    Méthode pour savoir si c'est un carré, avec un boolean
    */
    public boolean isCarre(){
        if(this.longueur == this.largeur) { return true; }
        else return false;
    }

    /*
    Méthode pour afficher les informations du rectangle
    */
    public String toString(){ return "Le rectangle a pour largeur "+this.largeur+" et de longueur "+this.longueur+". Son périmètre est de "+ this.Perim() + " et son aire est de "+ this.Aire() +". C'est un carré ? " + this.isCarre(); }

    /*
    Méthode pour vérifier si les 2 rectangles font le même périmètre ou la même aire
    */
    public boolean isEqual(Rectangle R){
        if((this.Aire() == R.Aire()) || (this.Perim() == R.Perim())) {return true;}
        else return false;
    }

    public static void main(String[] args){
        /*
        Création des objets rectangle en utilisant le constructeur
        */
        Rectangle Rec1 = new Rectangle(3,3);
        Rectangle Rec2 = new Rectangle(4,10);

        /*
        Affichage des info. sur les rectangles en utilisant la méthode
        */
        System.out.println(Rec1.toString());
        System.out.println(Rec2.toString());

        /*
        Affichage de l'aire et du périmètre du rectangle 1 en utilisant la méthode
        */
        System.out.println("Rec1 Aire : "+ Rec1.Aire());
        System.out.println("Rec1 Périmètre : "+ Rec1.Perim());

        /*
        On vérifie si les rectangles sont des carrés ou non en utilisant la méthode
        */
        System.out.println("Rec2 est carré ? "+ Rec1.isCarre());
        System.out.println("Rec2 est carré ? "+ Rec2.isCarre());

        /*
        Vérification si les 2 rectangles ont la même aire/périmètre ou non en utilisant la méthode
        */
        System.out.println("Is Equal ? "+Rec1.isEqual(Rec2));
    }
}