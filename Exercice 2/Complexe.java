public class Complexe 
{
    private double reel;
    private double imaginaire;

    public Complexe(double r, double img){
        this.reel = r;
        this.imaginaire = img;
    }

    /*
    Permet de récupérer la partie réel du nombre
    */
    public double getReel(){ return this.reel; }

    /*
    Permet de récupérer la partie imaginaire du nombre
    */
    public double getImg(){ return this.imaginaire; }

    /*
    Permet de calculer le conjugué
    */
    public String conjugue(){ return this.reel+" + i "+this.imaginaire; }

    /*
    Permet d'additionner les 2 nombres complexes
    */
    public String addition(Complexe c){ return (this.reel+c.getReel())+" + i "+(this.imaginaire+c.getImg()); }

    /*
    Permet de multiplier les nombres complexes
    */
    public String multip(Complexe c){ return (this.reel*c.getReel() - (this.imaginaire*c.getImg())+" + i "+(this.reel*c.getImg()+this.imaginaire*c.getReel())); }

    /*
    Vérifie si l'égalité est vraie
    */
    public boolean isEqual(Complexe C1){
        if(this.addition(C1) == this.multip(C1)) return true;
        else return false;
    }

    public static void main(String[] args){

        //Création des nombres complexes
        Complexe C1 = new Complexe(3.0,3.0);
        Complexe C2 = new Complexe(4.0,10.0);

        //Calcul du conjugué en utilisant la méthode
        System.out.println(C1.conjugue());
        System.out.println(C2.conjugue());

        //Addition et multiplication des 2 nombres complexes
        System.out.println(C1.addition(C2));
        System.out.println(C1.multip(C2));

        //Vérification de l'égalité multiple == addition
        System.out.println(C1.isEqual(C2));
    }
}
